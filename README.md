# doc-public

#### 介绍
个人文档公开仓库
目前主要是对若依框架的文档持续更新

https://gitee.com/QSSSYH/doc-public/tree/master/若依框架入门开发手册


#### 软件说明
推荐使用Typora来打开编辑md文档 文档结构目录更加清晰

下载地址: https://gitee.com/QSSSYH/doc-public/blob/master/MarkDown编辑器/typora-setup-x64.exe
![输入图片说明](https://images.gitee.com/uploads/images/2019/0919/233329_79b9417d_2201740.jpeg "u=2759417242,772332114&fm=26&gp=0.jpg")